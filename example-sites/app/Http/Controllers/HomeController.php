<?php

namespace App\Http\Controllers;

use Session;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $posts = Post::all();
        
        return view ('index')->with('posts', $posts);
}

     public function detalle (){
        $nombre = 'Andres';
        $apellido = 'Torres';
        $tel = '47789627';
        $email = 'andres@hotmail.com';

        return view('detalle')
     ->with('nombre' , $nombre)
     ->with('apellido' , $apellido)
     ->with('tel' , $tel)
     ->with('email' , $email);

     }

    public function store(Request $request)
    {
    $post = new Post;

    $post->title = $request->title;
    $post->body = $request->body;
    $post->author = $request->author;
    $post->date = $request->date;
    $post->keywords = $request->keywords;


    }

     public function show(Post $post)
    {
        return view ('posts.show');
    }

   }

