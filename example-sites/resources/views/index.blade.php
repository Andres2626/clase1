<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mi página web</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="custom.css">

</head>
@if(Session::has('exito'))
<div style="background: lightgrey; color: #fff;">
    <p>{{ Session::get('exito') }}</p>
</div>
@endif

@if(Session::has('del'))
<div style="background: red; color: #fff;">
    <p>{{ Session::get('del') }}</p>
</div>
@endif

<body>


    
    <form method="POST" action="{{  route('blog.store')  }}">
    {{ csrf_field() }}
<section class="menu">
    
<div class="container">
    <div class="row">
    <div class="col-md-6">
        <h4>Titulo</h4>
    <input type="text" name="title" required="">
    <br><br>
    </div>
    <div class="col-md-6">
        <h4>Publicacion</h4>
    <textarea name="body" required="" rows="5"></textarea>
    <br><br>
    </div>
    <div class="col-md-6">
        <h4>Fecha de publicación</h4>
    <input type="date" name="date">
    <br><br>
    </div>
    <div class="col-md-6">
        <h4>Palabra(s) clave</h4>
    <input type="text" name="keywords">
    <br><br>
    </div>

    <button type="submit">Guardar datos</button>
    </form>
    
    </div>
</div>

</section>

<section class="portafolio">
    <div class="container">
        <div class="row">
            
                @foreach($posts as $post)
            <div class="col-md-3">
               <h1><strong>{{  $post->title  }}</strong></h1>
               <a>{{  $post->body  }}</a>
               <p>{{  $post->author  }}</p>
               <form method="POST" action="{{ route('blog.destroy', $post->id) }}">
               <button type="submit">Borrar</button>
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               </form>
               <hr style="color: blue;">
            </div>

               @endforeach
        </div>
    </div>
</section>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
      <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js">
    </script>

</body>