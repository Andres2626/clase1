@if (Session::has('exito'))
<div style="background: lightgreen; color:#fff;">
    <p>{{ Session::get('exito') }}</p>
</div>
@endif

@if (Session::has('del'))
<div style="background: lightgreen; color:#fff;">
    <p>{{ Session::get('del') }}</p>
</div>
@endif

<form method="POST" action="{{ route ('blog.store') }}">
    {{ csrf_field () }}

    <label></label>Titulo</label><br>
    <input type="text" name="title" required=""><br>

    <label>Cuerpo</label><br>
    <textarea name="body" required="" rows="10"></textarea><br>

    <label>Autor</label><br>
    <input type="text" name="author"><br>

    <label>Fecha</label><br>
    <input type="date" name="date"><br>

    <label>Palabras clave</label>
    <input type="text" name="keywords"><br>
    <br>
    <br>
    <button type="submit">Guardar Datos</button>
</form>

@foreach($posts as $post)

    <h3>{{  $post->title  }}</h3>
    <p>{{  $post->body  }}</p>
    <form method="POST" action="{{ route('blog.destroy', $post->id) }}">
        <button type="submit">Borrar</button>
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
    <hr style="color: blue;">

@endforeach